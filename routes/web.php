<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PostController::class, 'index']);

//Route::resource('post', PostController::class);
Route::resource('post', PostController::class);

//// Show all posts:
//Route::get('post/', [PostController::class, 'index'])->name('post.index');
//// Create, save (store) and show the concretely post:
//Route::get('post/create', [PostController::class, 'create'])->name('post.create');
//Route::post('post/', [PostController::class, 'store'])->name('post.store');
//Route::get('post/show/{id}', [PostController::class, 'show'])->name('post.show');
//// Edit, update and destroy the concretely post:
//Route::get('post/edit/{id}', [PostController::class, 'edit'])->name('post.edit');
//Route::patch('post/show/{id}', [PostController::class, 'update'])->name('post.update');
//Route::delete('post/{id}', [PostController::class, 'destroy'])->name('post.destroy');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
