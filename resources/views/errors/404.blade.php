@extends('layouts.layout', ['title' => "404"])

@section('content')
    <div class="card">
        <h2 class="card-header">К сожалению, страница не найдена ╮( ˘ ､ ˘ )╭ </h2>
        <img src="{{ asset('img/404.jpg') }}" alt="404. Not found">
    </div>

    <a href="/" class="btn btn-outline-primary">Срочно вернуться на главную</a>
@endsection